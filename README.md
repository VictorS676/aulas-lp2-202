## Linguagem de Programação WEB 2

** Primeira Aula (19/10):**
	
1.0. Apresentação.

- Objetivo;
- Metodologia;
- Registro de participação;
- Avaliação.

1.1. Ferramentas necessárias.

- XAMPP;
- Git;
- VSCode;
- BitBucket.

1.2. Por quê usar framework?

- Codeigniter;
- MDBootstrap.

1.3. Configurações.

- Downloads;
- Inclusão do MDB no CodeIgniter;
- Versionamento do código;
- Configurações do CodeIgniter.

##### Hands On - 01

    Tenha configurado o ambiente de desenvolvimento;
    Tenha feito o versionamento do seu código;
    Tenha feito as configurações iniciais do CodeIgniter.

------------

###### * Atividades Realizadas *

- Download dos Frameworks CodeIgniter 3 e MDBootstrap 4;
- Criação do projeto e do repositório da disciplina no BitBucket;
- Versionamento dos códigos que serão usados em aula.

** Segunda Aula (26/10):**

2.0. Iniciando a programação.

- Mais um commit;
- Estrutura do CodeIgniter;
- Introdução ao uso de controladores;
- Introdução ao uso de views;
- Controlador padrão.

2.1. Criação de páginas.

- Criação de páginas / métodos;
- Endereçamento de páginas;
- Organização das views;
- Acesso aos assets do MDB.

2.2. Organização do HTML.

- Introdução às classes do MDB;
- Introdução ao uso de forms.

##### Hands On - 02

    Tenha configurado o acesso aos assets do MDB;
    Saiba como obter e organizar código vindo do MDB;
    Consiga criar e acessar novas páginas;
    Entenda como postar um formulário;
    Tenha feito commit de todo seu código;
    Envio do repositório do BitBucket no AVA.

 + *URL Exemplo:* http://localhost/lp2/victor_pedro/Controlador/metodo/

------------

 3.0. Introdução do uso dos models.

- Criação de models;
- Carregamento de models;
- Usando métodos dos models;
- Métodos públicos;
- Estratégia de post interno.

3.1. Operações lógicas no lugar certo.

- Lógica de login;
- Redirecionamento no login;
- Mais sobre controladores;
- Reutilização de código;
- Mensagem de erro no login;
- Passagem de parâmetros para views;
- Uso de váriáveis passadas para as views;
- Operador ternário na views.

3.2. Navegação entre páginas.

- Barras de navegação;
- Definição dos itens de menu;
- Criação dos links de navegação;
- Conceito de dashboard;
- Remoção do index.php.

##### Hands On - 03

    Entenda a finalidade do uso de models;
    Entenda a estratégia de POST interno;
    Saiba como criar e configurar um menu;
    Remova o index.php dos seus URLs;
    Entenda a reutilização de views;
    Commit de todo seu código.

------------

4.0. Herança vs repetição de código.

- Herança;
- Entendendo controlador;
- Métodos da classe pai;
- Carregando views como strings;
- Concatenação de strings;
- Parâmetro com valor padrão.

4.1. Sabores de MVC.

- MVC: fat model vs fat controller;
- Separação de responsabilidades;
- Motivação do uso de libraries;
- Configuração do BD;
- Tabela login no BD;
- Objeto db do Codeigniter;
- Active Record no CI;
- Segurança na leitura do POST.

4.2. Libraries: A quarta camada de abstração.

- Criação de libraries;
- Criação de classes;
- Acesso aos métodos do CI;
- Classe CI_Object.

##### Hands On - 04

    Entenda a finalidade do uso de libraries;
    Entenda a herança como meio de evitar repetir código;
    Saiba carregar views como strings;
    Tenha criado a método básico de todas as páginas;
    Tenha configurado ao acesso ao BD corretamente;
    Saiba o que é Active Record;
    Entenda a necesside de herança na criação de libraries.

------------

5.0. CRUD Orientado a Objetos - Parte 01.

- Prática de Desenvolvimento.

------------

6.0. CRUD Orientado a Objetos - Parte 02.

    6.1. Modelagem de Classes.

    - A necessidade da criação de componentes;
    - Modelagem de classes e refatoração;
    - Uso de métodos auxiliares.

    6.2. Validação.

    - Introdução à validação de dados.

------------

7.0. CRUD Orientado a Objetos - Parte 03.

    7.1. Mais sobre validação.

    - Organização da validação;
    - Re-população de formulário;
    - Exibição de mensagens de erro;
    - Tradução das mensagens de erro.

    7.2. API Rest no PHP.

    - Configuração da API REST;
    - Endereçamento pelo JavaScript;
    - Classe Rest de exemplo;
    - Classe Model de exemplo;
    - Primeiras chamadas ao PHP.

    7.2. Integrando Ajax ao PHP.

    - Habilitando os botões de deletar;
    - Cuidado: um erro comum;
    - Removendo o registro no DB;
    - Modal para confirmar remoção.

------------

8.0. CRUD Orientado a Objetos - Parte 04.

    8.1. Editando via REST.

    8.2. AJAX com BD.

------------

9.0. Generalização e Refatoração.

------------

10.0. Introdução ao Teste de Sistemas.
    - Teste automatizado & unitário (para uma estrutura - método - ou unidade do sistema);
    - End-to-end test;
    - PHPUnit - framework p/ testes unitários;
    - TDD;
    - Teste de Regressão;
    - Teste de Integração (SQL);
    - Testes independentes;

------------

11.0. Desenvolvimento de teste.

------------
