/**
 * Recebe o nome de um controlador a de um método.
 * Retorna o url do site atual que executa este método
 * 
 * @parem ctrl - nome do controlador
 * @parem func - método a ser executado
 * @returns {String}
 * 
 */
function baseURL(uri) {
    return 'http://' + window.location.hostname +
        '/lp2/victor_pedro' + (uri ? '/' + uri : '');
}

/**
 * Gera o url da página
 */
function api(ctrl, func) {
    return baseURL('api/' + ctrl + 'Rest/' + func);
}