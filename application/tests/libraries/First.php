<?php

class First extends TestCase{
    public function setUp(): void{
        $this->resetInstance();
    }

    /*
    function xtestHello(){
        $this->CI->load->library('conta');
        $res = $this->CI->conta->soma(3, 2);
        $this->assertEquals(5, $res);
    }

    function xtestContaDeveRetornarRegistrosDoMesDezembro(){
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 12, 2020);
        $this->assertEquals(4, sizeof($res));
    }*/

    function xtestContaDeveInserirRegistroCorretamente(){
        // Cenário
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        // Ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 1, 2021);

        // Verificação
        $this->assertEquals('Magalu', $res[0]['parceiro']);
        $this->assertEquals(2021, $res[0]['ano']);
        $this->assertEquals(1, $res[0]['mes']);
        $this->assertEquals(3, sizeof($res));

        $this->assertEquals('Casas Bahia', $res[0]['parceiro']);
        $this->assertEquals(2021, $res[0]['ano']);
        $this->assertEquals(1, $res[0]['mes']);

        $this->assertEquals('Bandeirante', $res[4]['parceiro']);
        $this->assertEquals(97.25, $res['valor']);
        $this->assertEquals(2021, $res[4]['ano']);
        $this->assertEquals(1, $res[4]['mes']);;
    }
    
    function testContaDeveInsformarTotalDeContasApagarEAReceber(){
        // Cenário
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        // Ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->total('pagar', 1, 2021);

        //Verificação
        $this->assertEquals(5097.25, $res);
    }

    function testContaDeveCalcularSaldoMensal(){
        //Cenário
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        // Ação
        $this->CI->load->library('conta');
        $res = $this->CI->conta->saldo(1, 2021);

        //Verificação
        $this->assertEquals(-875.07, $res);
    }
}
